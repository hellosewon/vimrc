call plug#begin()
Plug '/usr/local/opt/fzf'
Plug 'altercation/vim-colors-solarized'
Plug 'ambv/black', { 'for': 'python' }
Plug 'junegunn/fzf.vim'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-eunuch'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-rsi'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-sleuth'
Plug 'tpope/vim-surround'
call plug#end()

au BufWritePre *.py Black

function! ClangFormat()
  let l:formatdiff = 1
  py3f /usr/local/opt/clang-format/share/clang/clang-format.py
endfunction
au BufWritePre *.h,*.cc,*.cpp,*.c call ClangFormat()

se bg=dark
silent! colorscheme solarized

let mapleader="\<Space>"
nnoremap <leader>T :se t_ti= t_te=<cr>
nnoremap <leader>b :Buffers<cr>
nnoremap <leader>d :Files ~<cr>
nnoremap <leader>f :Files<cr>
nnoremap <leader>: :map <lt>leader>t :w \\|

se cursorline
se encoding=utf8
se expandtab
se hlsearch
se noswapfile
se nowrap
se number
se showcmd
se showmatch
se visualbell
